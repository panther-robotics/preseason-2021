// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.commands.*;
import frc.robot.subsystems.*;
import frc.robot.commands.DriveCommand;
import frc.robot.subsystems.Drivetrain;
import frc.robot.subsystems.Lift;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj.XboxController;




/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  private Command m_autonomousCommand;

  private RobotContainer m_robotContainer;
  public Drivetrain drivetrain;
  public static Collector collector;
  public static Shooter shooter;
  public static Limelight limelight;
  public static float hv=0;
  public static Lift lift;
  public static XboxController xboxDrive;



  //public static CommandScheduler scheduler;

  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    // Instantiate our RobotContainer.  This will perform all our button bindings, and put our
    // autonomous chooser on the dashboard.
    OI.xboxDrive = new XboxController(RobotMap.XBOX_PORT);
    m_robotContainer = new RobotContainer();
    drivetrain = new Drivetrain();
    collector = new Collector();
    shooter = new Shooter();
    limelight = new Limelight();

    lift = new Lift();


    // JoystickButton startButton = new JoystickButton(xboxDrive, 8);
    // startButton.whenPressed(new ExtendLift(lift));
    // JoystickButton backButton = new JoystickButton(xboxDrive, 7);
    // backButton.whenPressed(new RetractLift(lift));
    CommandScheduler.getInstance().registerSubsystem(lift);
    CommandScheduler.getInstance().setDefaultCommand(lift, new ExtendLift(lift));
    CommandScheduler.getInstance().setDefaultCommand(lift, new RetractLift(lift));
    CommandScheduler.getInstance().registerSubsystem(drivetrain);
    CommandScheduler.getInstance().setDefaultCommand(drivetrain, new DriveCommand(drivetrain));
    CommandScheduler.getInstance().registerSubsystem(drivetrain);
    CommandScheduler.getInstance().setDefaultCommand(drivetrain, new DriveCommand(drivetrain));
    CommandScheduler.getInstance().registerSubsystem(collector);
    CommandScheduler.getInstance().setDefaultCommand(collector, new CollectCommand(collector));
    CommandScheduler.getInstance().registerSubsystem(shooter);
    CommandScheduler.getInstance().setDefaultCommand(shooter, new ShooterCommand(shooter));
    CommandScheduler.getInstance().registerSubsystem(limelight);
    CommandScheduler.getInstance().setDefaultCommand(limelight, new LimelightCommand(limelight));
  }

  /**
   * This function is called every robot packet, no matter the mode. Use this for items like
   * diagnostics that you want ran during disabled, autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before LiveWindow and
   * SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {
    // Runs the Scheduler.  This is responsible for polling buttons, adding newly-scheduled
    // commands, running already-scheduled commands, removing finished or interrupted commands,
    // and running subsystem periodic() methods.  This must be called from the robot's periodic
    // block in order for anything in the Command-based framework to work.
    CommandScheduler.getInstance().run();
  }

  /** This function is called once each time the robot enters Disabled mode. */
  @Override
  public void disabledInit() {}

  @Override
  public void disabledPeriodic() {}

  /** This autonomous runs the autonomous command selected by your {@link RobotContainer} class. */
  @Override
  public void autonomousInit() {
    //m_autonomousCommand = m_robotContainer.getAutonomousCommand();

    // schedule the autonomous command (example)
    // if (m_autonomousCommand != null) {
    //   m_autonomousCommand.schedule();
    // }
  }

  /** This function is called periodically during autonomous. */
  @Override
  public void autonomousPeriodic() {}

  @Override
  public void teleopInit() {
    // This makes sure that the autonomous stops running when
    // teleop starts running. If you want the autonomous to
    // continue until interrupted by another command, remove
    // this line or comment it out.
    if (m_autonomousCommand != null) {
      m_autonomousCommand.cancel();
    }
  }

  /** This function is called periodically during operator control. */
  @Override
  public void teleopPeriodic() {}

  @Override
  public void testInit() {
    // Cancels all running commands at the start of test mode.
    CommandScheduler.getInstance().cancelAll();
  }

  /** This function is called periodically during test mode. */
  @Override
  public void testPeriodic() {}
}
