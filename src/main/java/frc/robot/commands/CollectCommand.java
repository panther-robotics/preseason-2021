package frc.robot.commands;

import frc.robot.OI;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.subsystems.Collector;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.NeutralMode;

import edu.wpi.first.wpilibj2.command.CommandBase;
public class CollectCommand extends CommandBase{
    @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
    
    private final Collector m_subsystem;

    public static WPI_TalonSRX storageMotor;

    public CollectCommand(Collector subsystem) {
        m_subsystem = subsystem;
        // Use addRequirements() here to declare subsystem dependencies.
        addRequirements(subsystem);
    }
    public void initialize() {
        storageMotor = new WPI_TalonSRX(RobotMap.BALL_STORAGE_MOTOR_PORT);
        storageMotor.setNeutralMode(NeutralMode.Brake);
    }

    @Override
    public void execute() {
        if (OI.getRightBumper()){
            Robot.collector.extendSolenoid();
            Robot.collector.runCollector((float).9);
            storageMotor.set((double)1);
        }else{
            Robot.collector.runCollector(0);
            Robot.collector.retractSolenoid();
            storageMotor.set((double)0);
        }
    }    
}
