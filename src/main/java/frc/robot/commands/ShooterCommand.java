package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.OI;
import frc.robot.Robot;
import frc.robot.subsystems.*;

public class ShooterCommand extends CommandBase {
    @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
    private final Shooter m_subsystem;
    /**
    * Creates a new ExampleCommand.
    *
    * @param subsystem The subsystem used by this command.
    */
    public ShooterCommand(Shooter subsystem) {
        m_subsystem = subsystem;
        addRequirements(subsystem);
    }

    public int tickWaitThing = 0;

    @Override
    public void initialize() {}

    @Override
    public void execute() {
        if (OI.getY()) {Robot.shooter.toShootOrNotToShoot(1,tickWaitThing);tickWaitThing+=1;}
        else {Robot.shooter.toShootOrNotToShoot(0,tickWaitThing);tickWaitThing = 0;}
    }

    @Override
    public void end(boolean interrupted) {}
    @Override
    public boolean isFinished() {return false;}
}