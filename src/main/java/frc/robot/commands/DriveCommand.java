// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import frc.robot.subsystems.Drivetrain;
import frc.robot.OI;
import frc.robot.Robot;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj2.command.CommandBase;

/** An example command that uses an example subsystem. */
public class DriveCommand extends CommandBase {

  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private final Drivetrain m_subsystem;

  /**
   * Creates a new ExampleCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public DriveCommand(Drivetrain subsystem) {
    m_subsystem = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    // leftMotors = new SpeedControllerGroup(topLeftMotor,midLeftMotor,bottomLeftMotor);
    // rightMotors = new SpeedControllerGroup(topRightMotor,midRightMotor,bottomRightMotor);
    // drive = new DifferentialDrive(leftMotors, rightMotors);
  }

  public void drive(float speed) {
    Drivetrain.topLeftMotor.set(-speed);Drivetrain.midLeftMotor.set(-speed);
    Drivetrain.bottomLeftMotor.set(-speed);Drivetrain.topRightMotor.set(speed);
    Drivetrain.midRightMotor.set(speed);Drivetrain.bottomRightMotor.set(speed);
  }

  // public void turn(float speed) {
  //   Drivetrain.topLeftMotor.set(speed);
  //   Drivetrain.midLeftMotor.set(speed);
  //   Drivetrain.bottomLeftMotor.set(speed);
  //   Drivetrain.topRightMotor.set(speed);
  //   Drivetrain.midRightMotor.set(speed);
  //   Drivetrain.bottomRightMotor.set(speed);
  // }

  public void move_and_turn(float forward_speed, float horizontal_speed) {
    Drivetrain.topLeftMotor.set((-forward_speed+horizontal_speed)/2);
    Drivetrain.midLeftMotor.set((-forward_speed+horizontal_speed)/2);
    Drivetrain.bottomLeftMotor.set((-forward_speed+horizontal_speed)/2);
    Drivetrain.topRightMotor.set((forward_speed+horizontal_speed)/2);
    Drivetrain.midRightMotor.set((forward_speed+horizontal_speed)/2);
    Drivetrain.bottomRightMotor.set((forward_speed+horizontal_speed)/2);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (OI.getLeftStickY() >= 0.2 || OI.getLeftStickY() <= -0.2 || OI.getLeftStickX() <= -0.2 || OI.getLeftStickX() >= 0.2) {
      move_and_turn((float)OI.getLeftStickY()*0.9f, (float)OI.getLeftStickX()*0.9f);
    } else if(Robot.hv > 0 || Robot.hv < 0 && OI.getB()) {move_and_turn(0, Robot.hv);} else {drive(0);}
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
