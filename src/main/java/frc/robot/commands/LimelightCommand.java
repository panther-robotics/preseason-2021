package frc.robot.commands;

import frc.robot.subsystems.Limelight;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.NetworkTable;
import frc.robot.Robot;

public class LimelightCommand extends CommandBase {
   @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
   private final Limelight m_subsystem;

   /**
   * Creates a new ExampleCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
    public LimelightCommand(Limelight subsystem) {
    m_subsystem = subsystem;
        addRequirements(subsystem);
    }

    NetworkTable table = NetworkTableInstance.getDefault().getTable("limelight");

    public void thing() {
        table.getEntry("getpipe").setNumber(0);
        double x = table.getEntry("tx").getDouble(0.0);
        double y = table.getEntry("ty").getDouble(0.0);
        double area = table.getEntry("ta").getDouble(0.0);
        if(x > 1 && area > 3) {Robot.hv = 0.3f;}
        else if(x < -1 && area > 3) {Robot.hv = -0.3f;}
        System.out.println(table.getEntry("tv").getDouble(0.0));
    }

    @Override
    public void initialize() {}
    @Override
    public void execute() {table.getEntry("getpipe").setNumber(0);
    double x = table.getEntry("tx").getDouble(0.0);
    double y = table.getEntry("ty").getDouble(0.0);
    double area = table.getEntry("ta").getDouble(0.0);
    Robot.hv = 0;
    if(x > 5 && area > 1) {Robot.hv = 0.3f;}
    else if(x < -5 && area > 1) {Robot.hv = -0.3f;}
    System.out.println(table.getEntry("tv").getDouble(0.0));}
    @Override
    public void end(boolean interrupted) {}
    @Override
    public boolean isFinished() {return false;}

}