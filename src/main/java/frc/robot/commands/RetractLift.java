package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.OI;
import frc.robot.subsystems.Lift;

public class RetractLift extends CommandBase{
    
@Override
  public void end(boolean interrupted) {

  }
    Lift lift;
    
  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return lift.unravelWinch();
  }
  public RetractLift(Lift subsystem) {
    lift = subsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

  }
  public void execute(){
      if(OI.getA()){
          Robot.lift.ravelWinch();
      }
  }

}