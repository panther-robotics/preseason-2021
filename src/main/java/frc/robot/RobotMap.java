package frc.robot;

public class RobotMap {

    //xbox controller:
    public static final int XBOX_PORT = 3;

    //collector solenoid:
    public static final int COLLECTOR_SOLENOID_PORT_FORWARDS = 3;
    public static final int COLLECTOR_SOLENOID_PORT_BACKWARDS = 4;

    //collector motors:
    public static final int COLLECTOR_BOTTOM_MOTOR_PORT = 19;    
    public static final int COLLECTOR_MID_MOTOR_PORT = 7;
    public static final int COLLECTOR_TOP_MOTOR = 8;

    public static final int BALL_STORAGE_MOTOR_PORT = 17;

    public static final int SHOOTING_MOTOR_ONE = 6;
    public static final int SHOOTING_MOTOR_TWO = 4;


    //wheel ports:
    public static final int TOP_LEFT_MOTOR = 11;
    public static final int MID_LEFT_MOTOR = 12;
    public static final int BOT_LEFT_MOTOR = 13;
    public static final int TOP_RIGHT_MOTOR = 14;
    public static final int MID_RIGHT_MOTOR = 15;
    public static final int BOT_RIGHT_MOTOR = 16;

    //lift ports:
    public static final int LIFT_WINCH_MOTOR = 18;
}
