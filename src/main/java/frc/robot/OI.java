package frc.robot;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;

public class OI {
    
    public static XboxController xboxDrive;

    public static double getLeftStickY() {return xboxDrive.getY(Hand.kLeft);}
    public static double getLeftStickX() {return xboxDrive.getX(Hand.kLeft);}
    
    public static double getRightStickY() {return xboxDrive.getY(Hand.kRight);}
    public static double getRightStickX() {return xboxDrive.getX(Hand.kRight);}

    public static boolean getLeftBumper() {return xboxDrive.getBumper(Hand.kLeft);}
    public static boolean getRightBumper() {return xboxDrive.getBumper(Hand.kRight);}

    public static boolean getA() {return xboxDrive.getAButton();};
    public static boolean getB() {return xboxDrive.getBButton();};
    public static boolean getX() {return xboxDrive.getXButton();};
    public static boolean getY() {return xboxDrive.getYButton();};

}