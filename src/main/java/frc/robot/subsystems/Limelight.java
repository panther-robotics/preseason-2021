package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Robot;

public class Limelight extends SubsystemBase {

    public Limelight() {}

    public void thing() {
        NetworkTable table = NetworkTableInstance.getDefault().getTable("limelight");
        double x = table.getEntry("tx").getDouble(0.0);
        double y = table.getEntry("ty").getDouble(0.0);
        double area = table.getEntry("ta").getDouble(0.0);
        if(x >= (320/2)) {Robot.hv = 0.3f;}
        else if(x < (320/2)) {Robot.hv = -0.3f;}
    }

    @Override
    public void periodic() {}
    @Override
    public void simulationPeriodic() {}

}