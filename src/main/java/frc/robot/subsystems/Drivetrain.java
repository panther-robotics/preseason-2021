// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

public class Drivetrain extends SubsystemBase {
  
  public static  WPI_TalonFX topLeftMotor;
  public static  WPI_TalonFX midLeftMotor;
  public static  WPI_TalonFX bottomLeftMotor;
  public static  WPI_TalonFX topRightMotor;
  public static  WPI_TalonFX midRightMotor;
  public static  WPI_TalonFX bottomRightMotor;

  public Drivetrain() {
    topLeftMotor = new WPI_TalonFX(RobotMap.TOP_LEFT_MOTOR);
    midLeftMotor = new WPI_TalonFX(RobotMap.MID_LEFT_MOTOR);
    bottomLeftMotor = new WPI_TalonFX(RobotMap.BOT_LEFT_MOTOR);
    topRightMotor = new WPI_TalonFX(RobotMap.TOP_RIGHT_MOTOR);
    midRightMotor = new WPI_TalonFX(RobotMap.MID_RIGHT_MOTOR);
    bottomRightMotor = new WPI_TalonFX(RobotMap.BOT_RIGHT_MOTOR);
    topLeftMotor.setNeutralMode(NeutralMode.Brake);
    midLeftMotor.setNeutralMode(NeutralMode.Brake);
    bottomLeftMotor.setNeutralMode(NeutralMode.Brake);
    topRightMotor.setNeutralMode(NeutralMode.Brake);
    midRightMotor.setNeutralMode(NeutralMode.Brake);
    bottomRightMotor.setNeutralMode(NeutralMode.Brake);
  }

  @Override
  public void periodic() {
    
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
}
