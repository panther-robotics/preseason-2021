// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;
import frc.robot.RobotMap;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

public class Collector extends SubsystemBase {
  /** Creates a new ExampleSubsystem. */


    //port 8 is the TalonSRX collector motor; port 3 is solenoid forward, port 4 is solenoid back
  WPI_VictorSPX collectorMotorTop = new WPI_VictorSPX(RobotMap.COLLECTOR_TOP_MOTOR);
  WPI_VictorSPX collectorMotorGreenWheel = new WPI_VictorSPX(RobotMap.COLLECTOR_MID_MOTOR_PORT);
  WPI_TalonSRX collectorMotorConveyorBelt = new WPI_TalonSRX(RobotMap.COLLECTOR_BOTTOM_MOTOR_PORT);
  

  DoubleSolenoid collectorSolenoid = new DoubleSolenoid(RobotMap.COLLECTOR_SOLENOID_PORT_FORWARDS,RobotMap.COLLECTOR_SOLENOID_PORT_BACKWARDS);


  public Collector() {
    collectorMotorTop.set(0);
    collectorMotorTop.setNeutralMode(NeutralMode.Brake);

  }
  public void extendSolenoid(){collectorSolenoid.set(Value.kForward);}
  public void retractSolenoid(){collectorSolenoid.set(Value.kReverse);}
  public void runCollector(float speed){
    collectorMotorTop.set(-speed);
    collectorMotorGreenWheel.set(-speed);
    collectorMotorConveyorBelt.set(speed);
    
  }

  
  @Override
  public void periodic() {
    
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
}
