package frc.robot.subsystems;

import frc.robot.RobotMap;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;
import com.revrobotics.CANEncoder;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.ControlType;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

public class Shooter extends SubsystemBase {

    CANSparkMax shootingMotorOne = new CANSparkMax(RobotMap.SHOOTING_MOTOR_ONE, MotorType.kBrushless);
    CANSparkMax shootingMotorTwo = new CANSparkMax(RobotMap.SHOOTING_MOTOR_TWO, MotorType.kBrushless);
    WPI_VictorSPX collectorMotorGreenWheel = new WPI_VictorSPX(RobotMap.COLLECTOR_MID_MOTOR_PORT);

    public static WPI_TalonSRX storageMotor;

    public Shooter() {
        shootingMotorTwo.set(0);
        shootingMotorOne.set(0);
        shootingMotorOne.setIdleMode(IdleMode.kBrake);
        shootingMotorTwo.setIdleMode(IdleMode.kBrake);
        shootingMotorOne.setInverted(true);
        storageMotor = new WPI_TalonSRX(RobotMap.BALL_STORAGE_MOTOR_PORT);
        storageMotor.setNeutralMode(NeutralMode.Brake);
    }

    public void toShootOrNotToShoot(float v, int i) {
        shootingMotorOne.set(v);
        shootingMotorTwo.set(v);
        if(v != 0 && i >= 50) {
            storageMotor.set(-v);
            collectorMotorGreenWheel.set(v);
        } else if(v == 0) {
            storageMotor.set(-v);
            collectorMotorGreenWheel.set(v);
        }
    }

    @Override
    public void periodic() {}
    @Override
    public void simulationPeriodic() {}

}