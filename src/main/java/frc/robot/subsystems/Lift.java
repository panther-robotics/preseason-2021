// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotMap;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.revrobotics.CANEncoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

public class Lift extends SubsystemBase {
  
  private CANSparkMax winchMotor = new CANSparkMax(RobotMap.LIFT_WINCH_MOTOR,MotorType.kBrushed);
  private CANEncoder winchEncoder;
  
  public Lift() {
    winchMotor.setIdleMode(IdleMode.kBrake);
    winchEncoder = winchMotor.getEncoder();
    winchEncoder.setPositionConversionFactor(winchEncoder.getCountsPerRevolution());
    winchEncoder.setPosition(0);
  }

  @Override
  public void periodic() {
    
  }

  public boolean unravelWinch(){
    winchMotor.set(0.1);
    return winchEncoder.getPosition() >= 6;
  }

  public boolean ravelWinch(){
    winchMotor.set(-0.1);
    return winchEncoder.getPosition() <= 1;
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }
}
